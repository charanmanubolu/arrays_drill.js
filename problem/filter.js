
function filter(elements, cb) {
    // Do NOT use .filter, to complete this function.
    // Similar to `find` but you will return an array of all elements that passed the truth test
    // Return an empty array if no elements pass the truth test
    let result = []
    try {
        for (let index = 0; index < elements.length; index++) {
            if (cb(elements[index], index, elements)) {
                result.push(elements[index])
            }
        }
    } catch (error) {
        console.error(error);
    }
    return result
}

module.exports = filter