function each(elements, cb) {

    try {
        for (let index = 0; index < elements.length; index++) {
            cb(elements[index], index)
        }
    } catch (error) {
        console.log(error)
    }

}

function square(ele, index) {
    return ele * ele

}

module.exports ={ each,square}


